package com.cy.java.oop;
public class IntegerTests{
    public static void main(String[] args) {
        //演示整数池(-128~+127)
        Integer n1=100;//Integer.valueOf(100)
        Integer n2=100;
        //对整数而言为什么不将所有整数都放到池中,而只是存储了一部分数据呢?
        //池设计的目的是?以空间换时间,这块空间中应该存储一些常用的整数数据
        Integer n3=200;//new Integer(200)
        Integer n4=200;
        System.out.println(n1==n2);//true
        System.out.println(n3==n4);//true
        //所有池的设计都会用到一种设计模式:享元模式 (通过池复用对象)
    }
}


