package com.cy.pj.sys.service;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogTests {
    private static final Logger log=
            LoggerFactory.getLogger(LogTests.class);
    @Test
    void testLevel(){
         //日志级别从低到高： trace<debug<info<warn<error
         //当我们在springboot配置文件中指定的日志级别为info时，会显示info,warn,error相关信息
         log.trace("log.level.trace");
         log.debug("log.level.debug");
         log.info("log.level.info");
         log.warn("log.level.warn");
         log.error("log.level.error");
         //日志级别的配置可以在springboot配置文件中通过logging.level进行实现
        //假如希望将日志写到指定目录的文件中可以通过logging.file.path
    }
}
