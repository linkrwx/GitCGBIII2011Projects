package com.cy.pj.sys.dao;

import com.cy.pj.sys.pojo.SysNotice;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SysNoticeDaoProxyTests {

    @Autowired //属性的之由spring进行依赖注入
    private SysNoticeDaoProxy sysNoticeDao;

    @Test
    void testInsertNotice(){
        //创建SysNotice对象,通过此对象封装要写入到数据库的数据
        SysNotice notice=new SysNotice();
        notice.setTitle("CGB2009结课时间");
        notice.setContent("2021/3/20正式结课");
        notice.setStatus("0");
        notice.setType("1");
        notice.setCreatedUser("tony");
        notice.setModifiedUser("tony");
        //将SysNotice对象持久化到数据库
        sysNoticeDao.insertNotice(notice);//此方法的实现内部会通过SQLSession向表中写数据。
    }
}
