package com.cy.pj.sys.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 通过此单元测试类获取数据源对象,并且通过数据对象获取数据库连接
 * @SpringBootTest 注解描述的类
 * 为springboot中的单元测试类
 * 说明:
 * 1)springboot中的单元测试类必须放在启动类所在包
 * 或子包中
 * 2)springboot中的单元测试类必须使用@SpringBootTest注解描述
 */
@SpringBootTest
public class DataSourceTests {//is a Object
    /**
     * 在项目中添加了数据库相关依赖以后,springboot底层会自动帮我们配置一个
     * 数据源(DataSource)对象,此对象是连接池的规范.
     * @Autowired注解描述属性时,是告诉spring框架,要基于反射机制为属性赋值(依赖注入)
     * 赋值时,首先会基于属性类型从spring容器查找相匹配的对象, 假如只有一个则直接注入,
     * 有多个相同类型的对象时,还会比较属性名(检测属性名是否与bean名字相同),有相同的
     * 则直接注入(没有相同的直接抛出异常.)
     */
    @Autowired
    private DataSource dataSource;//HikariDataSource (类)

    @Test //org.junit.jupiter.api.Test
    void testGetConnection() throws SQLException {
        //获取链接时,会基于dataSource获取连接池对象,进而从池中获取连接
        Connection conn=dataSource.getConnection();
        System.out.println("conn="+conn);
    }


}
