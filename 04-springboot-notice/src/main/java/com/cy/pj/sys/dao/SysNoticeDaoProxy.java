package com.cy.pj.sys.dao;

import com.cy.pj.sys.pojo.SysNotice;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

/**@Repository注解由spring框架提供，用于描述数据逻辑对象，
 * 同样这样的对象会交给spring去管理，是spring容器中的一个bean*/
//@Repository
public class SysNoticeDaoProxy {//模拟mybatis为Dao接口产生的实现类
    @Autowired
    private SqlSession sqlSession;
    public int insertNotice(SysNotice notice){
        String statement="com.cy.pj.sys.dao.SysNoticeDao.insertNotice";
        //数据持久化(底层会基于statement找到对应的sql)
        int rows=sqlSession.insert(statement,notice);
        return rows;
    }
}
