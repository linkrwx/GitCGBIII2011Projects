package com.cy.pj.sys.service.impl;

import com.cy.pj.common.annotation.RequiredLog;
import com.cy.pj.common.annotation.RequiredTime;
import com.cy.pj.sys.dao.SysNoticeDao;
import com.cy.pj.sys.pojo.SysNotice;
import com.cy.pj.sys.service.SysNoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * @Service注解由spring提供，一般用于描述分层架构中的业务逻辑对象，这样的类
 * 会交给spring管理
 */
@Service
public  class SysNoticeServiceImpl implements SysNoticeService {
    /**创建日志门面API对象*/
    private static final Logger log=
            //通过工厂创建日志对象
            LoggerFactory.getLogger(SysNoticeServiceImpl.class);

    @Autowired
    private SysNoticeDao sysNoticeDao;
    //重写方法的生成 (选中类,然后alt+enter)
    @RequiredLog(operation = "新增公告信息")
    @Override
    public int saveNotice(SysNotice notice) {
        int rows=sysNoticeDao.insertNotice(notice);
        return rows;
    }
    //RequiredLog注解描述的方法为日志切入点方法
    @RequiredLog(operation = "查询公告列表")
    @Override
    public List<SysNotice> findNotices(SysNotice notice) {
        String tName=Thread.currentThread().getName();
        System.out.println("SysNoticeService.findNotices.threadName="+tName);
        List<SysNotice> list=sysNoticeDao.selectNotices(notice);
        return list;
    }
    @Override
    public int deleteById(Long... ids) {
        //检查用户权限
        //开启事务
        //log.debug("start: {}",System.currentTimeMillis());
        int rows= sysNoticeDao.deleteById(ids);
        if(rows==0)throw new RuntimeException("记录可能已经不存在");
        return rows;
    }
    //@RequiredCache
    @RequiredLog(operation = "基于id查询通知")
    @RequiredTime
    @Override
    public SysNotice findById(Long id) {
        SysNotice notice=sysNoticeDao.selectById(id);
        //long r=id/0;
        return notice;
    }

    @Override
    public int updateNotice(SysNotice notice) {
        //检查用户权限
        //开启事务
        //log.debug("start: {}",System.currentTimeMillis());
        int rows=sysNoticeDao.updateNotice(notice);
        return rows;
    }
}
